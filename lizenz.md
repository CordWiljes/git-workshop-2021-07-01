# Lizenz

![CC-BY-SA-4.0](images/by-sa.svg) [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)

Dieser Kurs nutzt Inhalte aus 

* M. Politze (2021): Einstieg ins Forschungsdatenmanagement mit git und GitLab
* Software Carpentry: [Version Control with Git](https://swcarpentry.github.io/git-novice/)