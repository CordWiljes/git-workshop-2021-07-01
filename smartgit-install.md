# Schritt 4 (otional): SmartGit installieren

SmartGit ist ein Desktop-Client, d.h. ein Programm, das lokal auf Ihrem Rechner installiert wird. Es erleichtert die Arbeit mit dem lokalen Git-Repositorium, indem es eine grafische Benutzeroberfläche (GUI) zur Verfügung stellt. Sie können aber auch ohne einen solchen Desktop-Client, nur in der Shell mit Git interagieren. Selbst wenn Sie später evtl. mit einem GUI arbeiten sollten, ist es für das Verständnis der Vorgänge in Git erfarungsgemäß sehr hilfreich zunächst inige Zeit in der Shell zu arbeiten.

Es gibt eine Vielzahl GUIs für Git. Hier sei examplarisch der SmartGit-Client vorgestellt, der für Windows, MacOS und LInus verfügbar ist. Für die Nutzung in Lehre und Forschung ist er kostenlos. 

![SmartGit](images/SmartGit.png)