# Summary

* [Home](README.md)
* Vorbereitung
  1. [1. GitLab-Account aktivieren](gitlab-account-aktivieren.md)
  1. [2. Git installieren](git-installieren.md)
  1. [3. Erste Schritte](erste-schritte.md)
  1. [4. Desktop-Client installieren](desktop-client-installieren.md) (optional)
* [Links](links.md)
* [Lizenz](lizenz.md)
* [Kontakt](kontakt.md)

