# Vorbereitung

## Git auf Ihrem Rechner installieren

### Kurzanleitung
1.	Gehen Sie auf https://git-scm.com/downloads  und laden Sie die Installationsdateien Ihres Betriebssystems herunter. 
2.	Installieren Sie Git durch Doppelklick auf die Install-Datei. Bestätigen Sie die vorgeschlagenen Einstellungen.
3.	Starten Sie das Programm „Git Bash“ (bzw. unter Linux einfach die Linux Shell) und geben Sie ein:“

`git --version`

Als Ergebnis sollte dann z.B. angezeigt werden:
`git version 2.26.0.windows.1`


### Film-Anleitungen

* [Installation, Windows (1:53 min)](https://www.youtube.com/watch?v=5KFn0r2XrtA)
* [Installation, Mac OSX (2:49 min)](https://www.youtube.com/watch?v=TvrZw47e7tI)
* [Installation, Linux (2:13 min)](https://www.youtube.com/watch?v=HSOuBmiCdrM)
