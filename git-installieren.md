# 2. Git installieren

In diesem Punkt installieren Sie GitLab auf Ihre Rechner. 

1. Gehen Sie auf https://git-scm.com/downloads und laden Sie die Install-Datei Ihres Betriebssystems herunter. 
1. Installieren Sie Git durch Doppelklick auf die Install-Datei. Bestätigen Sie die vorgeschlagenen Einstellungen.
1. Starten Sie das Programm „Git Bash“ (bzw. unter Linux einfach die Linux Shell) und geben Sie ein:
   ```git --version```

Das Ergebnis sollte sein `git version 2.26.0.windows.1`


## Video-Anleitungen

* [Installation, Mac OSX (2:49)](https://www.youtube.com/watch?v=TvrZw47e7tI)
* [Installation, Windows (1:53)](https://www.youtube.com/watch?v=5KFn0r2XrtA)
* [Installation, Linux (2:13)](https://www.youtube.com/watch?v=HSOuBmiCdrM)

