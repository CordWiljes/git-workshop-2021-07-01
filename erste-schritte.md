# 3. Erste Schritte

## 1. Legen Sie ein Projekt in GitLab an

  * Loggen Sie sich in das GitLab ihrer Einrichtung ein
  * Erstellen Sie ein neues Projekt mit dem Namen "git-workshop"
  * Initialisieren Sie das Projekt mit einer `README.txt` Datei
  * Klicken Sie auf die `README.txt` um den Inhalt der Datei anzusehen
  * Klicken Sie auf den Button `Edit` und nahmen Sie eine Änderung an der `README.txt` vor
  * Gehen Sie zurück auf die Projektseite, klicken Sie dort auf den Button `Clone` und kopieren sie die URL, die unter `Clone with HTTPS` angezeigt wird.

## 2. Erstellen Sie eine lokale Kopie des Projektes

  * Öffnen Sie die Kommandozeile um mit git auf ihrem Computer zu arbeiten.
  * Erstellen Sie einen neuen, leeren Ordner. Zum Beispiel mit dem Kommando  
    ```mkdir gitlab-nrw-wokshop```
  * Wechseln Sie in den Ordner mit dem Kommando  
    ```cd workshop```
  * Laden Sie das Projekt herunter mit dem Kommando  
    ```git clone [URL]```

## 3. Versionieren Sie eine Datei lokal und übertragen Sie die Änderung an GitLab

  * Nehmen Sie eine Änderung in der Datei `README.txt` vor, diesmal in der lokalen Kopie des Repositories
  * Öffnen Sie Ihre Console, in Windows durch `Start` -> `Git Bash` 
  * Versionieren Sie die Änderung lokal mit dem Befehl 
    ```git add README.txt```
    ```git commit -m "README.txt first update"```
  * Übertragen sie die Änderung an GitLab mit dem Kommando  
    ```git push origin master```
  * Überprüfen Sie im Browser, ob die Änderung in GitLab angekommen ist.

Was die Befehle bedeuten, was hier genau geschieht, und warum man das so macht, werden Sie dann im Workshop erfahren.

Sollten Sie Fragen oder Probleme haben, stellen Sie bitte ein Ticket ein, wie unter [Kontakt](kontakt) beschrieben. 